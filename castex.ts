import { isTemplateSpan, NumberLiteralType } from "typescript";

// see https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Basic_animations, you moron
const canvas = document.querySelector('canvas');
const ctx = canvas?.getContext('2d');
// const ctx = document.getCSSCanvasContext("2d", "")

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

const width = window.innerWidth;
const height = window.innerHeight;

// const sprites : Array<Image> = [
const sprites = [
    new Image(),
    new Image(),
    new Image(),
    new Image(),
    new Image(),
];

sprites[0].src = 'res/img/castex_opa.png'
sprites[1].src = 'res/img/castex2_opa.png'
sprites[2].src = 'res/img/castex3_opa.png'
sprites[3].src = 'res/img/castex4_opa.png'
sprites[4].src = 'res/img/castex5_opa.png'

// const width = canvas?.width = window.innerWidth;
// const height = canvas?.height = window.innerHeight;

class FlyingCastex {
    x: number;
    y: number;
    dx: number;
    dy: number;
    sprite: HTMLImageElement;

    constructor(sprite: HTMLImageElement, x=0, y=0, dx=1, dy=1) {
        this.sprite = sprite;
        this.x = x;
        this.y = y;
        this.dx = dx;
        this.dy = dy;
    }

    update() {
        if ((this.x >= width) || (this.x <= 0)) {
            this.dx *= -1;
        }
        if ((this.y >= height) || (this.y <= 0)) {
            this.dy *= -1;
        }
        this.x += this.dx;
        this.y += this.dy;
    }
}

class FlyingCastexPool {
    castexes: Array<FlyingCastex>;

    constructor(x_bounds: [number ,number], y_bounds: [number, number], pool_size=5) {
        this.castexes = [...Array(pool_size)].map(castex => {
            return new FlyingCastex(
                sprites[Math.floor(gen_random(0, sprites.length))],
                gen_random(x_bounds[0], 
                                        x_bounds[1]), 
                                    gen_random(y_bounds[0],
                                        y_bounds[1]),
                                    gen_random(-.25, .25),
                            gen_random(-.25, .25)
            );
        });
    }
}

// return a number in [min, max[
function gen_random(min: number, max: number) : number {
    return Math.random() * (max-min) + min
}

let x_bounds : [number, number] = [0, canvas.width];
let y_bounds : [number, number] = [0, canvas.height];

let castex_pool = new FlyingCastexPool(x_bounds, y_bounds, 5);

function loop() {
    ctx?.clearRect(0, 0, width, height);
    ctx.fillStyle = 'rgba(0, 0, 0, 1)'
    castex_pool.castexes.forEach(element => {
        ctx?.drawImage(element.sprite, element.x, element.y);
        element.update();
    });
    ctx?.fill();
    requestAnimationFrame(loop);
}

loop();