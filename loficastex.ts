import { start } from "repl";

const YTPlayer = require('yt-player');
const player_options = {
    "related": false,
    // "host": "https://youtube-nocookie.com"
};

const castex_player = new YTPlayer('#player_castex', player_options);
const lofi_player = new YTPlayer('#player_lofi', player_options);

const castex_volume = document.querySelector("#castex-volume");
console.log(castex_volume);
const lofi_volume = document.querySelector("#lofi-volume");
console.log(lofi_volume);

const start_button = document.getElementById("start");

const castex_video_ids : Array<string> = new Array(
    // "VvX7OubU4oc",
    //"EGpGtyJaHS8",
    //"jHg87LAeiDk",
    //"8y5dbN5n0Vc",
    //"a1ih9NPr5qQ",
    "NRxWc1GbBqM"
)

const lofi_video_ids : Array<string> = new Array(
    "jfKfPfyJRdk"
)

// load videos
castex_player.load(castex_video_ids[0]);
lofi_player.load(lofi_video_ids[0]);

//set music a bit quieter
//TODO: see if we can get actual volume from user,
//      in order not to blast eardrums
lofi_player.setVolume(10);

//link start buttons to site button
start_button?.addEventListener("click", (e: Event) => {
    const castex_container : HTMLElement = document.querySelector('#container_castex');
    if (castex_container?.style.getPropertyValue("display") == "none") {
        castex_container.style.setProperty("display", "block");
    }
    toggle_videos();
});

//wire volume
castex_volume?.addEventListener("change", function(e: Event) {
    const target = e.target as HTMLInputElement;
    castex_player.setVolume(target.value);
});

lofi_volume?.addEventListener("change", function(e: Event) {
    const target = e.target as HTMLInputElement;
    lofi_player.setVolume(target.value);
});

function toggle_videos() {
    let castex_state = castex_player.getState();
    let lofi_state = lofi_player.getState();

    // if both are playing, stop both
    if ((castex_state != 'playing') && (lofi_state != 'playing')) {
        start_button.textContent = "stop stp";
        castex_player.play();
        lofi_player.play();

    // if state is unstarted, we're expecting a live that hasn't started yet
    // so play/pause only on lofi
    } else if (castex_state == 'unstarted') {
        start_button.textContent = "ça n'a pas encore commencé 🌚";
        if (lofi_state == "playing") {
            lofi_player.pause();
        } else {
            lofi_player.play();
        }
    }
    else {
        start_button.textContent = "let's go les annonces!";
        castex_player.pause();
        lofi_player.pause();
    }
}

//
const canvas = document.querySelector("canvas");
const ctx = canvas?.getContext("2d");
