ALL:
	npx tsc loficastex.ts
	npx tsc castex.ts
	npx browserify loficastex.js castex.js -o bundle.js

CLEAN:
	rm bundle.js loficastex.js
